import { useState } from 'react';
import { useRouter } from 'next/router';
import { signIn } from '@lib/firebase';
import { useAuth } from '@contexts/auth';
import styles from '@styles/signin.module.scss';
import { Layout } from '@components';
import Footer from 'components/Footer/Footer';

const SignInPage = () => {
  const router = useRouter();
  const [user, userLoading] = useAuth();
  const [values, setValues] = useState({ email: '', password: '' });

  if (userLoading) {
    return <h1>Loading...</h1>;
  }

  if (user && typeof window !== 'undefined') {
    router.push('/');
    return null;
  }

  const handleChange = (e) => {
    const id = e.target.id;
    const newValue = e.target.value;

    setValues({ ...values, [id]: newValue });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    let missingValues = [];
    Object.entries(values).forEach(([key, value]) => {
      if (!value) {
        missingValues.push(key);
      }
    });

    if (missingValues.length > 1) {
      alert(`You're missing these fields: ${missingValues.join(', ')}`);
      return;
    }

    signIn(values.email, values.password).catch((err) => {
      alert(err);
    });
  };

  return (
    <Layout>
    <div className={styles.SignIn}>
      <form onSubmit={handleSubmit}>
        <h1>Please Sign In</h1>
        <label htmlFor="email">Email</label>
        <input
          id="email"
          type="email"
          value={values.email}
          onChange={handleChange}
          required
        />
        <label htmlFor="password">Password</label>
        <input
          id="password"
          type="password"
          value={values.password}
          onChange={handleChange}
          required
        />
        <button type="submit">Sign In</button>
      </form>
       <br />
       <br />
       <br />
      <form action="https://getform.io/f/9761fcd0-f687-4d56-97fa-07a51a8fa26d" method="POST">
              <h1>You want to be a member?</h1>
              <p>You can send us a message of requirement with this form</p>
              <label for="Name">Name</label>
              <input type="text" name="name" placeholder="Name"  required />
              <label for="Email">Email</label>
              <input type="email" name="email" placeholder="Email" required />
              <label for="Message">Message</label>
              <textarea type="text" name="message" required></textarea>
              <button type="submit">Send</button>
      </form>
    </div>
    <br />
    <br />
    <Footer />
   </Layout>
  );
};

export default SignInPage;
