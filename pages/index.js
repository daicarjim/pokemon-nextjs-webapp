// This component represents the index page for the site. You
// can read more about Pages in the Next.js docs at:
// https://nextjs.org/docs/basic-features/pages

import { getPosts } from '@lib/firebase';
import { getFormattedDate } from '@lib/utils';
import { Layout } from '@components';
import styles from '@styles/index.module.scss';
import Search from 'components/Search/Search';
import Footer from 'components/Footer/Footer';

const HomePage = ({ posts }) => (
  <Layout>
    <Search />
    <ul className={styles.HomePage}>
      {posts.map((post) => (
        <li key={post.slug}>
          <a href={`/post/${post.slug}`}>
            <img src={post.coverImage} alt={post.coverImageAlt} /> 
          </a>
         
          <div>

             <h2>{post.title}</h2>

            <span>{getFormattedDate(post.dateCreated)}</span>

            <p
              dangerouslySetInnerHTML={{
                __html: `${post.content.substring(0, 70)}...`,
              }}
            ></p>
            <a href={`/post/${post.slug}`}>View more</a>

          </div>
        </li>
      ))}
    </ul>
    <br />
    <br />
   <Footer />
  </Layout>
);

export async function getServerSideProps() {
  const posts = await getPosts();

  return {
    props: {
      posts,
    },
  };
}

export default HomePage;
