import styles from './Footer.module.scss';

const Footer = () => {

  return (
   <footer className={styles.Footer}>
     <button><a href="https://www.daimoncardenas-developer.tk/" target="_blank">@Daimon Cardenas-Enero 2022</a></button>
   </footer>      
  );
};

export default Footer;

