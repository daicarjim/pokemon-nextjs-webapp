import styles from './Search.module.scss';


const Search = () => {


  return (
    <main className={styles.Search}>
    <form>
      <div>
        <input type="text" placeholder="Search" />
      </div>
        </form>
    </main>
  )
}

export default Search;
